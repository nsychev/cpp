# CPP course

Сорокин Иван Владимирович

[Страница курса](https://sorokin.github.io/cpp-course/)

* [2018-02-10: Assembly basics](20180210.md)
* [2018-02-17: Архитектура ЭВМ](20180217.md) | [Презентация](assets/20180217-presentation.pdf)
